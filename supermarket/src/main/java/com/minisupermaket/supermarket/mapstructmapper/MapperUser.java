package com.minisupermaket.supermarket.mapstructmapper;

import org.mapstruct.Mapper;

import com.minisupermaket.supermarket.dto.UserDTO;
import com.minisupermaket.supermarket.entity.User;

@Mapper(componentModel = "spring")
public interface MapperUser {

	User map(UserDTO dto);
	UserDTO map(User entity);
}
