package com.minisupermaket.supermarket.mapstructmapper;

import org.mapstruct.Mapper;

import com.minisupermaket.supermarket.dto.BuyDTO;
import com.minisupermaket.supermarket.entity.Buy;

@Mapper(componentModel = "spring")
public interface MapperBuy {
	Buy map(BuyDTO dto);
	BuyDTO map(Buy entity);
	
	
}
