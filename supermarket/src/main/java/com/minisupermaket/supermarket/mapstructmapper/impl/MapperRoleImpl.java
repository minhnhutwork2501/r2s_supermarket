package com.minisupermaket.supermarket.mapstructmapper.impl;

import org.springframework.stereotype.Component;

import com.minisupermaket.supermarket.dto.RoleDTO;
import com.minisupermaket.supermarket.entity.Role;
import com.minisupermaket.supermarket.mapstructmapper.MapperRole;

@Component
public class MapperRoleImpl implements MapperRole{

	@Override
	public Role map(RoleDTO dto) {
		if(dto == null) {
			return null;
		}
		
		Role entity = new Role();
		entity.setCreatDate(dto.getCreateDate());
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		return entity;
	}

	@Override
	public RoleDTO map(Role entity) {
		RoleDTO dto = new RoleDTO();
		dto.setCreateDate(entity.getCreatDate());
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		return dto;
	}

}
