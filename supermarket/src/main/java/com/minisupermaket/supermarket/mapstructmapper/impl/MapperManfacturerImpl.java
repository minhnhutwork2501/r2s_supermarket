package com.minisupermaket.supermarket.mapstructmapper.impl;

import org.springframework.stereotype.Component;

import com.minisupermaket.supermarket.dto.ManfacturerDTO;
import com.minisupermaket.supermarket.entity.Manfacturer;
import com.minisupermaket.supermarket.mapstructmapper.MapperManfacturer;

@Component
public class MapperManfacturerImpl implements MapperManfacturer{

	@Override
	public Manfacturer map(ManfacturerDTO dto) {
		if(dto == null) {
			return null;
		}
		
		Manfacturer entity = new Manfacturer();
		entity.setId(0);
		entity.setFullName(dto.getFullName());
		entity.setEmail(dto.getEmail());
		entity.setAddress(dto.getAddress());
		entity.setPhone(dto.getPhone());
		entity.setTaxCode(dto.getTaxCode());
		entity.setManager(dto.getManager());
		return entity;
	}

	@Override
	public ManfacturerDTO map(Manfacturer entity) {
		if(entity == null) {
			return null;
		}
		
		ManfacturerDTO dto = new ManfacturerDTO();
		dto.setId(entity.getId());
		dto.setFullName(entity.getFullName());
		dto.setEmail(entity.getEmail());
		dto.setAddress(entity.getAddress());
		dto.setPhone(entity.getPhone());
		dto.setTaxCode(entity.getTaxCode());
		dto.setManager(entity.getManager());
		return null;
	}

}
