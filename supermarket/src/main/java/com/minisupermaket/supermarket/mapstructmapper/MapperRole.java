package com.minisupermaket.supermarket.mapstructmapper;

import org.mapstruct.Mapper;

import com.minisupermaket.supermarket.dto.RoleDTO;
import com.minisupermaket.supermarket.entity.Role;

@Mapper(componentModel = "spring")
public interface MapperRole {

	Role map(RoleDTO dto);
	RoleDTO map(Role entity);
}
