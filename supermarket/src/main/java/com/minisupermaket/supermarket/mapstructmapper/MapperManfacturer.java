package com.minisupermaket.supermarket.mapstructmapper;

import org.mapstruct.Mapper;

import com.minisupermaket.supermarket.dto.ManfacturerDTO;
import com.minisupermaket.supermarket.entity.Manfacturer;

@Mapper(componentModel = "spring")
public interface MapperManfacturer {
	Manfacturer map(ManfacturerDTO dto);
	ManfacturerDTO map(Manfacturer entity);

}
