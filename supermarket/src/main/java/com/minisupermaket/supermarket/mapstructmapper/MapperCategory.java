package com.minisupermaket.supermarket.mapstructmapper;

import org.mapstruct.Mapper;

import com.minisupermaket.supermarket.dto.CategoryDTO;
import com.minisupermaket.supermarket.entity.Category;

@Mapper(componentModel = "spring")
public interface MapperCategory {
	Category map(CategoryDTO dto);
	CategoryDTO map(Category entity);
	

}
