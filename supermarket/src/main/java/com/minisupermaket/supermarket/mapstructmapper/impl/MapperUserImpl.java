package com.minisupermaket.supermarket.mapstructmapper.impl;

import org.springframework.stereotype.Component;

import com.minisupermaket.supermarket.dto.UserDTO;
import com.minisupermaket.supermarket.entity.User;
import com.minisupermaket.supermarket.mapstructmapper.MapperUser;

@Component
public class MapperUserImpl implements MapperUser{

	@Override
	public User map(UserDTO dto) {
		if(dto == null) {
			return null;
		}
		
		User entity = new User();
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		entity.setFullName(dto.getFullName());
		entity.setGender(dto.getGender());
		entity.setEmail(dto.getEmail());
		entity.setAddress(dto.getAddress());
		return entity;
	}

	@Override
	public UserDTO map(User entity) {
		if(entity == null) {
			return null;
		}
		
		UserDTO dto = new UserDTO();
		dto.setAddress(entity.getAddress());
		dto.setEmail(entity.getEmail());
		dto.setFullName(entity.getFullName());
		dto.setGender(entity.getGender());
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setPhone(entity.getPhone());
		return dto;
	}

}
