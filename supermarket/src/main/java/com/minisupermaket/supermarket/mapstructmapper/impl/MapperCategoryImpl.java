package com.minisupermaket.supermarket.mapstructmapper.impl;

import org.springframework.stereotype.Component;

import com.minisupermaket.supermarket.dto.CategoryDTO;
import com.minisupermaket.supermarket.entity.Category;
import com.minisupermaket.supermarket.mapstructmapper.MapperCategory;

@Component
public class MapperCategoryImpl implements MapperCategory{

	@Override
	public Category map(CategoryDTO dto) {
		if (dto == null) {
			return null;
		}
		
		Category entity = new Category();
		entity.setId(dto.getId());
		entity.setCode(dto.getCode());
		entity.setName(dto.getName());		
		return entity;
	}

	@Override
	public CategoryDTO map(Category entity) {
		if (entity == null) {
			return null;
		}
		
		CategoryDTO dto = new CategoryDTO();
		dto.setId(entity.getId());
		dto.setCode(entity.getCode());
		dto.setName(entity.getName());
		return dto;
	}

}
