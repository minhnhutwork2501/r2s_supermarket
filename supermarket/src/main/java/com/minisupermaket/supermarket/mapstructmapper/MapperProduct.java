package com.minisupermaket.supermarket.mapstructmapper;

import org.mapstruct.Mapper;

import com.minisupermaket.supermarket.dto.ProductDTO;
import com.minisupermaket.supermarket.entity.Product;

@Mapper(componentModel = "spring")
public interface MapperProduct {

	Product map(ProductDTO dto);
	ProductDTO map(Product entity);
}
