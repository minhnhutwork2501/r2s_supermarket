package com.minisupermaket.supermarket.mapstructmapper.impl;

import org.springframework.stereotype.Component;

import com.minisupermaket.supermarket.dto.ProductDTO;
import com.minisupermaket.supermarket.entity.Product;
import com.minisupermaket.supermarket.mapstructmapper.MapperProduct;

@Component
public class MapperProductImpl implements MapperProduct {

	@Override
	public Product map(ProductDTO dto) {
		if(dto == null) {
			return null;
		}
		
		Product entity = new Product();
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		entity.setPrice(dto.getPrice());
		entity.setExpirationDate(dto.getExpirationDate());
		return entity;
	}

	@Override
	public ProductDTO map(Product entity) {
		if(entity == null) {
			return null;
		}
		
		ProductDTO dto = new ProductDTO();
		dto.setExpirationDate(entity.getExpirationDate());
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setPrice(entity.getPrice());
		return dto;
	}

}
