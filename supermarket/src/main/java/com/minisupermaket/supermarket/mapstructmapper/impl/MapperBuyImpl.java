package com.minisupermaket.supermarket.mapstructmapper.impl;

import org.springframework.stereotype.Component;

import com.minisupermaket.supermarket.dto.BuyDTO;
import com.minisupermaket.supermarket.entity.Buy;
import com.minisupermaket.supermarket.mapstructmapper.MapperBuy;

@Component
public class MapperBuyImpl implements MapperBuy{

	@Override
	public Buy map(BuyDTO dto) {
		if (dto == null) {
			return null;
		}
		Buy entity = new Buy();
		entity.setId(dto.getId());
		entity.setQuantity(dto.getQuantity());
		entity.setCreateDate(dto.getCreateDate());
		return entity;
	}

	@Override
	public BuyDTO map(Buy entity) {
		if (entity == null) {
			return null;
		}
		
		BuyDTO dto = new BuyDTO();
		dto.setId(entity.getId());
		dto.setCreateDate(entity.getCreateDate());
		dto.setQuantity(entity.getQuantity());
		return dto;
	}

}
