package com.minisupermaket.supermarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.minisupermaket.supermarket.entity.Buy;

@Repository
public interface BuyRepository extends JpaRepository<Buy, Integer> {
	

}
