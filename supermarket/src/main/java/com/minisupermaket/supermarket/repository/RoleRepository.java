package com.minisupermaket.supermarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.minisupermaket.supermarket.entity.Role;

@Repository
public interface RoleRepository  extends JpaRepository<Role, Integer>{

}
