package com.minisupermaket.supermarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.minisupermaket.supermarket.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
