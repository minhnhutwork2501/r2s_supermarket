package com.minisupermaket.supermarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.minisupermaket.supermarket.entity.Manfacturer;

@Repository
public interface ManfacturerRepository extends JpaRepository<Manfacturer, Integer>{

}
