package com.minisupermaket.supermarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.minisupermaket.supermarket.dto.CategoryDTO;
import com.minisupermaket.supermarket.entity.Category;
import com.minisupermaket.supermarket.service.CategoryService;

@RestController
@RequestMapping(path = "/categorydemo")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@PostMapping
	public ResponseEntity<CategoryDTO> save(@RequestBody CategoryDTO dto){
		return ResponseEntity.ok(categoryService.save(dto));
	}
	
	@PutMapping
	public ResponseEntity<CategoryDTO> update(@RequestBody CategoryDTO dto){
		return ResponseEntity.ok(categoryService.update(dto));
	}
	
	@DeleteMapping(path = "/{id}")
	public void deleteById(@PathVariable(name = "id") int id) {
		categoryService.deleteById(id);
	}
	
	@GetMapping(path = "/{id}")
	public Category findById(@PathVariable(name = "id") int id){
		return categoryService.findById(id);
	}
	
	@GetMapping
	public ResponseEntity<List<CategoryDTO>> findAll(){
		return ResponseEntity.ok(categoryService.findAll());
	}
}
