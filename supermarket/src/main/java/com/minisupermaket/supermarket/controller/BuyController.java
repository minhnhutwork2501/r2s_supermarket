package com.minisupermaket.supermarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.minisupermaket.supermarket.dto.BuyDTO;
import com.minisupermaket.supermarket.entity.Buy;
import com.minisupermaket.supermarket.service.BuyService;

@RestController
@RequestMapping(path = "/buydemo")
public class BuyController {
	
	@Autowired
	private BuyService buyService;
	
	@PostMapping
	public ResponseEntity<BuyDTO> save(@RequestBody BuyDTO dto){
		return ResponseEntity.ok(this.buyService.save(dto));
	}
	
	@PutMapping
	public ResponseEntity<BuyDTO> update(@RequestBody BuyDTO dto){
		return ResponseEntity.ok(buyService.update(dto));
	}

	@GetMapping(path = "/{id}")
	public Buy findById(@PathVariable(name = "id") int id){
		return buyService.findById(id);
	}
	
	@GetMapping
	public ResponseEntity<List<BuyDTO>> findAll(){
		return ResponseEntity.ok(buyService.findAll());
	}
	
	@DeleteMapping(path = "/{id}")
	public void deleteById(@PathVariable(name = "id") int id) {
		buyService.deleteById(id);
	}
}
