package com.minisupermaket.supermarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.minisupermaket.supermarket.dto.ProductDTO;
import com.minisupermaket.supermarket.entity.Product;
import com.minisupermaket.supermarket.service.ProductService;

@RestController
@RequestMapping(path = "/productdemo")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@PostMapping
	public ResponseEntity<ProductDTO> save(@RequestBody ProductDTO dto){
		return ResponseEntity.ok(productService.save(dto));
	}
	
	@PutMapping
	public ResponseEntity<ProductDTO> update(@RequestBody ProductDTO dto){
		return ResponseEntity.ok(productService.update(dto));
	}
	
	@DeleteMapping(path = "/{id}")
	public void deleteById(@PathVariable(name = "id") int id) {
		productService.deleteById(id);
	}
	
	@GetMapping(path = "/{id}")
	public Product findById(@PathVariable(name = "id") int id){
		return productService.findById(id);
	}
	
	@GetMapping
	public ResponseEntity<List<ProductDTO>> findAll(){
		return ResponseEntity.ok(productService.findAll());
	}

}
