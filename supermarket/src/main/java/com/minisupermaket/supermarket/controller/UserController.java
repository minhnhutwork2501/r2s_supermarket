package com.minisupermaket.supermarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.minisupermaket.supermarket.dto.UserDTO;
import com.minisupermaket.supermarket.entity.User;
import com.minisupermaket.supermarket.service.UserService;

@RestController
@RequestMapping(path = "/userdemo")
public class UserController {

	@Autowired
	private UserService userService;
	
	@PostMapping
	public ResponseEntity<UserDTO> save(@RequestBody UserDTO dto){
		return ResponseEntity.ok(userService.save(dto));
	}
	
	@PutMapping
	public ResponseEntity<UserDTO> update(@RequestBody UserDTO dto){
		return ResponseEntity.ok(userService.update(dto));
	}
	
	@DeleteMapping(path = "/{id}")
	public void deleteById(@PathVariable(name = "id") int id) {
		userService.deleteById(id);
	}
	
	@GetMapping(path = "/{id}")
	public User findById(@PathVariable(name = "id") int id){
		return userService.findById(id);
	}
	
	@GetMapping
	public ResponseEntity<List<UserDTO>> findAll(){
		return ResponseEntity.ok(userService.findAll());
	}
}
