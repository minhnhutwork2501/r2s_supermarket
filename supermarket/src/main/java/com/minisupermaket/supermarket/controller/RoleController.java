package com.minisupermaket.supermarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.minisupermaket.supermarket.dto.RoleDTO;
import com.minisupermaket.supermarket.entity.Role;
import com.minisupermaket.supermarket.service.RoleService;

@RestController
@RequestMapping(path = "/roledemo")
public class RoleController {
	
	@Autowired
	private RoleService roleService;
	
	@PostMapping
	public ResponseEntity<RoleDTO> save(@RequestBody RoleDTO dto){
		return ResponseEntity.ok(roleService.save(dto));
	}
	
	@PutMapping
	public ResponseEntity<RoleDTO> update(@RequestBody RoleDTO dto){
		return ResponseEntity.ok(roleService.update(dto));
	}
	
	@DeleteMapping(path = "/{id}")
	public void deleteById(@PathVariable(name = "id") int id) {
		roleService.deleteById(id);
	}
	
	@GetMapping(path = "/{id}")
	public Role findById(@PathVariable(name = "id") int id){
		return roleService.findById(id);
	}
	
	@GetMapping
	public ResponseEntity<List<RoleDTO>> findAll(){
		return ResponseEntity.ok(roleService.findAll());
	}

}
