package com.minisupermaket.supermarket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.minisupermaket.supermarket.dto.ManfacturerDTO;
import com.minisupermaket.supermarket.entity.Manfacturer;
import com.minisupermaket.supermarket.service.ManfacturerService;

@RestController
@RequestMapping(path = "/manfacturerdemo")
public class ManfacturerController {
	
	@Autowired
	private ManfacturerService manfacturerService;
	
	@PostMapping
	public ResponseEntity<ManfacturerDTO> save(@RequestBody ManfacturerDTO dto){
		return ResponseEntity.ok(manfacturerService.save(dto));
	}

	@PutMapping
	public ResponseEntity<ManfacturerDTO> update(@RequestBody ManfacturerDTO dto){
		return ResponseEntity.ok(manfacturerService.update(dto));
	}
	
	@DeleteMapping(path = "/{id}")
	public void deleteById(@PathVariable(name = "id") int id) {
		manfacturerService.deleteById(id);
	}
	
	@GetMapping(path = "/{id}")
	public Manfacturer findById(@PathVariable(name = "id") int id){
		return manfacturerService.findById(id);
	}
	
	@GetMapping
	public ResponseEntity<List<ManfacturerDTO>> findAll(){
		return ResponseEntity.ok(manfacturerService.findAll());
	}
}
