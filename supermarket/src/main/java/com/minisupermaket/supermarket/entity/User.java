package com.minisupermaket.supermarket.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "User")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(length = 200)
	private String name;
	
	@Column(name = "pass_word",length = 400)
	private String passWord;
	
	@Column(name = "full_name",length = 200)
	private String fullName;
	
	@Column(length = 20)
	private String gender;
	
	@Column(length = 500)
	private String email;
	
	@Column(length = 30)
	private String phone;
	
	@Column(length = 500)
	private String address;
	
	@Column(length = 10)
	private int status;
	
	@Column(name = "create_date")
	private Date createDate;
	
	@ManyToOne
	@JoinColumn(name = "roleId")
	private Role roleId;
	
	@Column(name = "avatar",length = 200)
	private String avatar;
	
}
