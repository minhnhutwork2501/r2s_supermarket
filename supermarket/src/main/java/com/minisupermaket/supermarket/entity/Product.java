package com.minisupermaket.supermarket.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "Product")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "manufacturerId")
	private Manfacturer manufacturer;
	
	@Column(length = 200)
	private String name;
	
	@ManyToOne
	@JoinColumn(name = "categoryId")
	private Category categoryId;
	
	@Column(name = "date_of_manufacturer")
	private Date dateOfManufacturer;
	
	@Column(name = "expiration_date")
	private Date expirationDate;
	
	@Column(length = 1000)
	private String element;
	
	@Column(length = 200)
	private String label;
	
	@Column(name = "user_manual",length = 1000)
	private String userManual;
	
	@Column
	private int weight;
	
	@Column(length = 200)
	private String barcode;
	
	@Column
	private float price;
	
	@OneToMany(mappedBy = "product")
	private List<Buy> buys = new ArrayList<>();
}
