package com.minisupermaket.supermarket.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "Manfacturer")
public class Manfacturer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "full_name",length = 400)
	private String fullName;
	
	@Column(length = 500)
	private String email;
	
	@Column(length = 30)
	private String phone;
	
	@Column(length = 500)
	private String address;
	
	@Column(name = "tax_code")
	private int taxCode;
	
	@Column(length = 300)
	private String manager;
	
	@Column(name = "business_license",length = 100)
	private String businessLicense;
	
	@OneToMany(mappedBy = "manufacturer")
	private List<Product> pros = new ArrayList<>();

}
