package com.minisupermaket.supermarket.service;

import java.util.List;

import com.minisupermaket.supermarket.dto.ManfacturerDTO;
import com.minisupermaket.supermarket.entity.Manfacturer;

public interface ManfacturerService {
	ManfacturerDTO save(ManfacturerDTO dto);
	ManfacturerDTO update(ManfacturerDTO dto);
	void deleteById(Integer id);
	List<ManfacturerDTO> findAll();
	Manfacturer findById(Integer id);

}
