package com.minisupermaket.supermarket.service;

import java.util.List;

import com.minisupermaket.supermarket.dto.UserDTO;
import com.minisupermaket.supermarket.entity.User;

public interface UserService {
	UserDTO save(UserDTO dto);
	UserDTO update(UserDTO dto);
	void deleteById(Integer id);
	List<UserDTO> findAll();
	User findById(Integer id);

}
