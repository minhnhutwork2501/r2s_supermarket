package com.minisupermaket.supermarket.service;

import java.util.List;

import com.minisupermaket.supermarket.dto.BuyDTO;
import com.minisupermaket.supermarket.entity.Buy;

public interface BuyService {

	BuyDTO save(BuyDTO dto);
	BuyDTO update(BuyDTO dto);
	void deleteById(Integer id);
	Buy findById(Integer id);
	List<BuyDTO> findAll();
}
