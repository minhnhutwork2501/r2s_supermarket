package com.minisupermaket.supermarket.service;

import java.util.List;

import com.minisupermaket.supermarket.dto.ProductDTO;
import com.minisupermaket.supermarket.entity.Product;

public interface ProductService {
	ProductDTO save(ProductDTO dto);
	ProductDTO update(ProductDTO dto);
	void deleteById(Integer id);
	List<ProductDTO> findAll();
	Product findById(Integer id);
}
