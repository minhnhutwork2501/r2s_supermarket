package com.minisupermaket.supermarket.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.minisupermaket.supermarket.dto.UserDTO;
import com.minisupermaket.supermarket.entity.User;
import com.minisupermaket.supermarket.exception.NotFoundException;
import com.minisupermaket.supermarket.mapstructmapper.MapperUser;
import com.minisupermaket.supermarket.repository.UserRepository;
import com.minisupermaket.supermarket.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private MapperUser mapperUser;
	
	@Value("${msg}")
	String msg;
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDTO save(UserDTO dto) {
		User entity = this.mapperUser.map(dto);
		entity = userRepository.save(entity);
		return this.mapperUser.map(entity);
	}

	@Override
	public UserDTO update(UserDTO dto) {
		User entity = this.mapperUser.map(dto);
		entity = userRepository.save(entity);
		return this.mapperUser.map(entity);
	}

	@Override
	public void deleteById(Integer id) {
		try {
			userRepository.deleteById(id);
		} catch (Exception e) {
			throw new NotFoundException(msg);
		}
		
	}

	@Override
	public List<UserDTO> findAll() {
		return userRepository.findAll().stream().map(item -> this.mapperUser.map(item)).collect(Collectors.toList());
	}

	@Override
	public User findById(Integer id) {
		return userRepository.findById(id).orElseThrow(() -> new NotFoundException(msg));
	}

}
