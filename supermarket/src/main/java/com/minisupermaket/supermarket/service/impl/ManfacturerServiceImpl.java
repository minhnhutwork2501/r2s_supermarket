package com.minisupermaket.supermarket.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.minisupermaket.supermarket.dto.ManfacturerDTO;
import com.minisupermaket.supermarket.entity.Manfacturer;
import com.minisupermaket.supermarket.exception.NotFoundException;
import com.minisupermaket.supermarket.mapstructmapper.MapperManfacturer;
import com.minisupermaket.supermarket.repository.ManfacturerRepository;
import com.minisupermaket.supermarket.service.ManfacturerService;

@Service
public class ManfacturerServiceImpl implements ManfacturerService{

	@Value("${msg}")
	String msg;
	
	@Autowired
	private MapperManfacturer mapperManfacturer;
	
	@Autowired
	private ManfacturerRepository manfacturerRepository;
	
	@Override
	public ManfacturerDTO save(ManfacturerDTO dto) {
		Manfacturer entity = this.mapperManfacturer.map(dto);
		entity = manfacturerRepository.save(entity);
		return this.mapperManfacturer.map(entity);
	}

	@Override
	public ManfacturerDTO update(ManfacturerDTO dto) {
		Manfacturer entity = this.mapperManfacturer.map(dto);
		entity = manfacturerRepository.save(entity);
		return this.mapperManfacturer.map(entity);
	}

	@Override
	public void deleteById(Integer id) {
		try {
			manfacturerRepository.deleteById(id);
		} catch (Exception e) {
			throw new NotFoundException(msg);
		}
		
	}

	@Override
	public List<ManfacturerDTO> findAll() {
		return manfacturerRepository.findAll().stream().map(item -> this.mapperManfacturer.map(item)).collect(Collectors.toList());
	}

	@Override
	public Manfacturer findById(Integer id) {
		return manfacturerRepository.findById(id).orElseThrow(() -> new NotFoundException(msg));
	}

}
