package com.minisupermaket.supermarket.service;

import java.util.List;

import com.minisupermaket.supermarket.dto.RoleDTO;
import com.minisupermaket.supermarket.entity.Role;

public interface RoleService {
	RoleDTO save(RoleDTO dto);
	RoleDTO update(RoleDTO dto);
	void deleteById(Integer id);
	List<RoleDTO> findAll();
	Role findById(Integer id);
}
