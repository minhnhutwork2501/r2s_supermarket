package com.minisupermaket.supermarket.service;

import java.util.List;

import com.minisupermaket.supermarket.dto.CategoryDTO;
import com.minisupermaket.supermarket.entity.Category;

public interface CategoryService {
	CategoryDTO save(CategoryDTO dto);
	CategoryDTO update(CategoryDTO dto);
	void deleteById(Integer id);
	List<CategoryDTO> findAll();
	Category findById(Integer id);

}
