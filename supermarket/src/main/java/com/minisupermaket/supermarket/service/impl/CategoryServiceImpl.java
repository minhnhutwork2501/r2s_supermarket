package com.minisupermaket.supermarket.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.minisupermaket.supermarket.dto.CategoryDTO;
import com.minisupermaket.supermarket.entity.Category;
import com.minisupermaket.supermarket.exception.NotFoundException;
import com.minisupermaket.supermarket.mapstructmapper.MapperCategory;
import com.minisupermaket.supermarket.repository.CategoryRepository;
import com.minisupermaket.supermarket.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private MapperCategory mapperCategory;
	
	@Value("${msg}")
	String msg;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public CategoryDTO save(CategoryDTO dto) {
		Category entity = this.mapperCategory.map(dto);
		entity = categoryRepository.save(entity);
		return this.mapperCategory.map(entity);
	}

	@Override
	public CategoryDTO update(CategoryDTO dto) {
		Category entity = this.mapperCategory.map(dto);
		entity = categoryRepository.save(entity);
		return this.mapperCategory.map(entity);
	}

	@Override
	public void deleteById(Integer id) {
		try {
			categoryRepository.deleteById(id);
		} catch (Exception e) {
			throw new NotFoundException(msg);
		}
		
	}

	@Override
	public List<CategoryDTO> findAll() {
		return categoryRepository.findAll().stream().map(item -> this.mapperCategory.map(item)).collect(Collectors.toList());
	}

	@Override
	public Category findById(Integer id) {
		return categoryRepository.findById(id).orElseThrow(() -> new NotFoundException(msg));
	}

}
