package com.minisupermaket.supermarket.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minisupermaket.supermarket.dto.BuyDTO;
import com.minisupermaket.supermarket.entity.Buy;
import com.minisupermaket.supermarket.exception.NotFoundException;
import com.minisupermaket.supermarket.mapstructmapper.MapperBuy;
import com.minisupermaket.supermarket.repository.BuyRepository;
import com.minisupermaket.supermarket.service.BuyService;

@Service
public class BuyServiceImpl implements BuyService {
	
	@Autowired
	private MapperBuy mapperBuy;
	
	@Autowired
	private BuyRepository buyRepository;

	@Override
	public BuyDTO save(BuyDTO dto) {
		Buy entity = this.mapperBuy.map(dto);
		entity = buyRepository.save(entity);
		return this.mapperBuy.map(entity);
	}

	@Override
	public BuyDTO update(BuyDTO dto) {
		Buy entity = this.mapperBuy.map(dto);
		entity = buyRepository.save(entity);
		return this.mapperBuy.map(entity);
	}

	@Override
	public void deleteById(Integer id) {
		buyRepository.deleteById(id);	
	}

	@Override
	public Buy findById(Integer id) {
		return buyRepository.findById(id).orElseThrow(() -> new NotFoundException("khong tim thay"));
	}

	@Override
	public List<BuyDTO> findAll() {
		return buyRepository.findAll().stream().map(item -> this.mapperBuy.map(item)).collect(Collectors.toList());
	}

}
