package com.minisupermaket.supermarket.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.minisupermaket.supermarket.dto.RoleDTO;
import com.minisupermaket.supermarket.entity.Role;
import com.minisupermaket.supermarket.exception.NotFoundException;
import com.minisupermaket.supermarket.mapstructmapper.MapperRole;
import com.minisupermaket.supermarket.repository.RoleRepository;
import com.minisupermaket.supermarket.service.RoleService;

@Service
public class RoleServiceImpl  implements RoleService{

	@Autowired
	private MapperRole mapperRole;
	
	@Value("${msg}")
	String msg;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Override
	public RoleDTO save(RoleDTO dto) {
		Role entity = this.mapperRole.map(dto);
		entity = roleRepository.save(entity);
		return this.mapperRole.map(entity);
	}

	@Override
	public RoleDTO update(RoleDTO dto) {
		Role entity = this.mapperRole.map(dto);
		entity = roleRepository.save(entity);
		return this.mapperRole.map(entity);
	}

	@Override
	public void deleteById(Integer id) {
		try {
			roleRepository.deleteById(id);
		} catch (Exception e) {
			throw new NotFoundException(msg);
		}
		
	}

	@Override
	public List<RoleDTO> findAll() {
		return roleRepository.findAll().stream().map(item -> this.mapperRole.map(item)).collect(Collectors.toList());
	}

	@Override
	public Role findById(Integer id) {
		return roleRepository.findById(id).orElseThrow(() -> new NotFoundException(msg));
	}

}
