package com.minisupermaket.supermarket.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.minisupermaket.supermarket.dto.ProductDTO;
import com.minisupermaket.supermarket.entity.Product;
import com.minisupermaket.supermarket.exception.NotFoundException;
import com.minisupermaket.supermarket.mapstructmapper.MapperProduct;
import com.minisupermaket.supermarket.repository.ProductRepository;
import com.minisupermaket.supermarket.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{
	
	@Value("${msg}")
	String msg;
	
	@Autowired
	private MapperProduct mapperProduct;
	
	@Autowired
	private ProductRepository productRepository;

	@Override
	public ProductDTO save(ProductDTO dto) {
		Product entity = this.mapperProduct.map(dto);
		entity = productRepository.save(entity);
		return this.mapperProduct.map(entity);
	}

	@Override
	public ProductDTO update(ProductDTO dto) {
		Product entity = this.mapperProduct.map(dto);
		entity = productRepository.save(entity);
		return this.mapperProduct.map(entity);
	}

	@Override
	public void deleteById(Integer id) {
		try {
			productRepository.deleteById(id);
		} catch (Exception e) {
			throw new NotFoundException(msg);
		}
		
	}

	@Override
	public List<ProductDTO> findAll() {
		return productRepository.findAll().stream().map(item -> this.mapperProduct.map(item)).collect(Collectors.toList());
	}

	@Override
	public Product findById(Integer id) {
		return productRepository.findById(id).orElseThrow(() -> new NotFoundException(msg));
	}

}
