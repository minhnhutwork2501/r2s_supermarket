package com.minisupermaket.supermarket.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BuyDTO {

	private int id;
	private int quantity;
	private Date createDate;
	private int productId;
	private int userId;
}
