package com.minisupermaket.supermarket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

	private int id;
	private String name;
	private String fullName;
	private String gender;
	private String email;
	private String phone;
	private String address;
}
