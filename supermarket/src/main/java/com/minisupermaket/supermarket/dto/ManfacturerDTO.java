package com.minisupermaket.supermarket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManfacturerDTO {

	private int id;
	private String fullName;
	private String email;
	private String phone;
	private String address;
	private int taxCode;
	private String manager;
}
